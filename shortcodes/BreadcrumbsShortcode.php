<?php
namespace Grav\Plugin\Shortcodes;

use Grav\Common\Utils;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class BreadcrumbsShortcode extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('breadcrumbs', function(ShortcodeInterface $shortcode) {
            //? $content = $shortcode->getContent();
            return "{% include 'partials/breadcrumbs.html.twig' %}";
        });
    }
}
