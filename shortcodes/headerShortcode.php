<?php
namespace Grav\Plugin\Shortcodes;

use Grav\Common\Utils;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class headerShortcode extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('header', function(ShortcodeInterface $shortcode) {
            $content = $shortcode->getContent();
            return '<div class="header">'.$content.'</div>';
        });
    }
}
