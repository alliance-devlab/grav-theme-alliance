# Alliance Theme for Grav

## Installation
Install the alliance theme by cloning the [Alliance Theme](https://gitlab.com/alliance-devlab/grav-theme-alliance) into the ```$GRAV/user/themes``` directory.

This theme inherits from [Quark](https://github.com/getgrav/grav-theme-quark) and [Quark Open Publishing](https://github.com/hibbitts-design/grav-theme-quark-open-publishing) themes, which will also need to be installed into the ```themes``` directory.

Alternatively clone the [Alliance Skeleton]() directly in place of the ```$GRAV/user``` directory.
